package bunny

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/venini42/bunny/pkg/cache"
	"gitlab.com/venini42/bunny/pkg/cache/stores"
	"gitlab.com/venini42/bunny/pkg/rabbitmq"
	"gitlab.com/venini42/bunny/pkg/rabbitmq/topology"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type listener func(ctx context.Context) error

type Service struct {
	Name,
	BrokerUrl string
	Rabbit          rabbitmq.Client
	Logger          logrus.FieldLogger
	Cache           cache.Store
	healthCheckPort int
	resourcesWG     sync.WaitGroup
	listeners       []listener
}

func handleInterrupt(teardown func() error) {
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		logrus.Warn("SIGTERM received")
		err := teardown()
		if err != nil {
			logrus.Fatal(err)
			os.Exit(1)
		}
		os.Exit(0)
	}()
}

func NewService(opts ...ServiceOption) (*Service, error) {
	opt := &ServiceOptions{
		brokerUrl:       "amqp://guest:guest@localhost:5672/",
		logger:          logrus.New(),
		timeout:         5,
		cache:           stores.InMemory(),
		healthCheckPort: 8080,
	}
	for _, o := range opts {
		o(opt)
	}

	name := opt.name
	logger := opt.logger.WithField("name", name)

	rabbit, err := rabbitmq.NewClient(opt.brokerUrl, logger, opt.timeout)
	if err != nil {
		return nil, err
	}
	svc := Service{
		Name:            name,
		Rabbit:          rabbit,
		Logger:          logger,
		Cache:           opt.cache,
		healthCheckPort: opt.healthCheckPort,
	}
	return &svc, nil
}

func (s *Service) AddListener(exchange, topic string, handler rabbitmq.MessageHandler) {
	listener := func(ctx context.Context) error {
		queue := fmt.Sprintf("service-%s-%s-%s", s.Name, exchange, topic)
		s.Logger.Debugf("Declaring handler queue: %s", queue)
		err := s.Rabbit.Init(func(builder topology.Builder) error {
			queue, err := builder.Queue(queue,
				topology.QueueDurable(true),
			)
			if err != nil {
				return err
			}

			exc, err := builder.Exchange(exchange,
				topology.ExchangeKind(topology.TopicExchange),
				topology.ExchangeDurable(true),
			)
			if err != nil {
				return err
			}
			return builder.Binding(
				topology.BindingQueue(queue),
				topology.BindingExchange(exc),
				topology.BindingKey(topic),
			)
		})
		if err != nil {
			return err
		}

		errs := s.Rabbit.Listen(ctx, queue, true, false, false, false, nil, handler)

		s.resourcesWG.Add(1)
		go func() {
			select {
			case err := <-errs:
				s.Logger.Error(err)
			case <-ctx.Done():
				s.resourcesWG.Done()
				return
			}

		}()
		return nil
	}
	s.listeners = append(s.listeners, listener)
}

func (s *Service) Start() error {
	err := connectWithRetry(s.Logger, func() error {
		return s.Rabbit.Connect()
	}, 6, 1)
	if err != nil {
		return err
	}

	s.resourcesWG.Add(1)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	handleInterrupt(func() error {
		s.Logger.Info("Service shutdown initiated...")
		cancel()
		<-ctx.Done()
		err := s.Rabbit.Stop()
		s.resourcesWG.Done()
		if err != nil {
			return err
		}
		s.Logger.Info("Service shutdown completed. Bye!")
		return nil
	})

	for _, listener := range s.listeners {
		err := listener(ctx)
		if err != nil {
			cancel()
			<-ctx.Done()
			return err
		}
	}

	err = setupHealthChecks(s.healthCheckPort)
	if err != nil {
		cancel()
		<-ctx.Done()
		return err
	}

	s.Logger.Debugf("Initialized %d listeners", len(s.listeners))
	s.Logger.Infof("Started service %s, ready to serve requests", s.Name)
	s.resourcesWG.Wait()
	return nil
}

func connectWithRetry(logger logrus.FieldLogger, connect func() error, retries, wait int) error {
	logger.Debugf("Connecting with %d retries and waiting %d seconds", retries, wait)
	err := connect()
	if err == nil || retries == 0 {
		logger.Debug("Stopping retries", err, retries)
		return err
	}

	timer := time.NewTimer(time.Duration(wait) * time.Second)
	<-timer.C

	retries--
	wait = wait * 2
	logger.Infof("Trying again, %d retries left and waiting %d seconds", retries, wait)
	return connectWithRetry(logger, connect, retries, wait)
}
