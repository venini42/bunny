package bunny

import (
	"fmt"
	"github.com/etherlabsio/healthcheck"
	"net/http"
	"time"
)

func setupHealthChecks(port int) error {
	http.Handle("/health", healthcheck.Handler(
		healthcheck.WithTimeout(5*time.Second),
	))

	return http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}
