package tests

import "testing"

func FailOnError(t *testing.T) func(err error) {
	return func(err error) {
		if err != nil {
			t.Fatal(err)
		}
	}
}
