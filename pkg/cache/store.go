package cache

type Store interface {
	Set(key string, value interface{}) error
	Get(key string) (interface{}, error)
	GetBool(key string) (bool, error)
	GetFloat64(key string) (float64, error)
	GetInt(key string) (int, error)
	GetString(key string) (string, error)
	GetStringMap(key string) (map[string]string, error)
	GetStringSlice(key string) ([]string, error)
	Keys() ([]string, error)
}
