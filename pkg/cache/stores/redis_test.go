package stores

import (
	"github.com/gomodule/redigo/redis"
	"testing"
	"time"
)

type mockConn string

func (c mockConn) Do(key string, p ...interface{}) (interface{}, error) {
	switch key {
	case "GET":
		return string(c), nil
	default:
		return nil, nil
	}
}
func (c mockConn) DoWithTimeout(timeout time.Duration, cmd string, args ...interface{}) (interface{}, error) {
	return timeout, nil
}

func (c mockConn) Receive() (interface{}, error) {
	return time.Duration(-1), nil
}
func (c mockConn) ReceiveWithTimeout(timeout time.Duration) (interface{}, error) {
	return timeout, nil
}

func (c mockConn) Send(string, ...interface{}) error { return nil }
func (c mockConn) Err() error                        { return nil }
func (c mockConn) Close() error                      { return nil }
func (c mockConn) Flush() error                      { return nil }

func TestRedisCache_Set(t *testing.T) {
	pool := &redis.Pool{Dial: func() (redis.Conn, error) { return mockConn("world"), nil }}
	cache := &RedisCache{pool: pool}
	k, err := cache.Get("test")
	if err != nil {
		t.Error(err)
	}

	if k != "world" {
		t.Errorf("Expected key to equal \"world\", was %s", k)
	}
}
