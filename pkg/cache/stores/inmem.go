package stores

import (
	"errors"
	"fmt"
)

type InMemoryCache struct {
	cache map[string]interface{}
}

func (c *InMemoryCache) Keys() ([]string, error) {
	keys := make([]string, 0, len(c.cache))
	for k := range c.cache {
		keys = append(keys, k)
	}
	return keys, nil
}

func (c *InMemoryCache) Set(key string, value interface{}) error {
	c.cache[key] = value
	return nil
}

func (c *InMemoryCache) Get(key string) (interface{}, error) {
	k, ok := c.cache[key]
	if !ok {
		return nil, errorf("Key %v not found", k)
	}
	return k, nil
}

func (c *InMemoryCache) GetBool(key string) (bool, error) {
	k, err := c.Get(key)
	if err != nil {
		return false, err
	}
	v, ok := k.(bool)
	if !ok {
		return false, errorf("cast failed: %v of type %T is not compatible with bool", v, v)
	}
	return v, nil
}

func (c *InMemoryCache) GetFloat64(key string) (float64, error) {
	k, err := c.Get(key)
	if err != nil {
		return 0, err
	}
	v, ok := k.(float64)
	if !ok {
		return 0, errorf("cast failed: %v of type %T is not compatible with float64", v, v)
	}
	return v, nil
}

func (c *InMemoryCache) GetInt(key string) (int, error) {
	k, err := c.Get(key)
	if err != nil {
		return 0, err
	}
	v, ok := k.(int)
	if !ok {
		return 0, errorf("cast failed: %v of type %T is not compatible with int", v, v)
	}
	return v, nil
}

func (c *InMemoryCache) GetString(key string) (string, error) {
	k, err := c.Get(key)
	if err != nil {
		return "", err
	}
	v, ok := k.(string)
	if !ok {
		return "", errorf("cast failed: %v of type %T is not compatible with string", v, v)
	}
	return v, nil
}

func (c *InMemoryCache) GetStringSlice(key string) ([]string, error) {
	k, err := c.Get(key)
	if err != nil {
		return []string{}, err
	}
	v, ok := k.([]string)
	if !ok {
		return []string{}, errorf("cast failed: %v of type %T is not compatible with []string", v, v)
	}
	return v, nil
}

func (c *InMemoryCache) GetStringMap(key string) (map[string]string, error) {
	k, err := c.Get(key)
	if err != nil {
		return nil, err
	}

	v, ok := k.(map[string]string)
	if !ok {
		return nil, errorf("cast failed: %v of type %T is not compatible with map[string]string", v, v)
	}
	return v, nil
}

func InMemory() *InMemoryCache {
	return &InMemoryCache{cache: map[string]interface{}{}}
}

func errorf(format string, a ...interface{}) error {
	return errors.New(fmt.Sprintf(format, a...))
}
