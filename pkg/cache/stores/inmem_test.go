package stores

import (
	"fmt"
	"sort"
	"testing"
)

func TestInMemoryCache_Set(t *testing.T) {
	k, v := "hello", "world"
	cache := InMemoryCache{cache: map[string]interface{}{}}
	err := cache.Set(k, v)
	if err != nil {
		t.Fatalf("Set returned an error %s, expected to succeed", err.Error())
	}

	if val := cache.cache[k]; val != v {
		t.Errorf("Expected cache value \"%s\" to equal \"%s\", was %s", k, v, val)
	}
}

func TestInMemoryCache_Get(t *testing.T) {
	k, v := "hello", "world"
	cache := InMemoryCache{cache: map[string]interface{}{k: v}}
	val, err := cache.Get(k)
	if err != nil {
		t.Errorf("Expected key \"%s\" to exist, but it didn't", k)
	}
	if v != val {
		t.Errorf("Expected cache value \"%s\" to equal \"%s\", was %s", k, v, val)
	}
}

func TestInMemoryCache_GetString(t *testing.T) {
	k, v := "hello", "world"
	cache := InMemoryCache{cache: map[string]interface{}{k: v}}
	val, err := cache.GetString(k)
	if err != nil {
		t.Errorf("Expected key \"%s\" to exist, but it didn't", k)
	}
	if v != val {
		t.Errorf("Expected cache value \"%s\" to equal \"%s\", was %s", k, v, val)
	}
}

func TestInMemoryCache_GetBool(t *testing.T) {
	k, v := "cool", true
	cache := InMemoryCache{cache: map[string]interface{}{k: v}}
	val, err := cache.GetBool(k)
	if err != nil {
		t.Errorf("Expected key \"%s\" to exist, but it didn't", k)
	}
	if v != val {
		t.Errorf("Expected cache value \"%s\" to equal \"%v\", was %v", k, v, val)
	}
}

func TestInMemoryCache_GetInt(t *testing.T) {
	k, v := "universe", 42
	cache := InMemoryCache{cache: map[string]interface{}{k: v}}
	val, err := cache.GetInt(k)
	if err != nil {
		t.Errorf("Expected key \"%s\" to exist, but it didn't", k)
	}
	if v != val {
		t.Errorf("Expected cache value \"%s\" to equal \"%d\", was %d", k, v, val)
	}
}

func TestInMemoryCache_GetFloat64(t *testing.T) {
	k, v := "radio", 102.5
	cache := InMemoryCache{cache: map[string]interface{}{k: v}}
	val, err := cache.GetFloat64(k)
	if err != nil {
		t.Errorf("Expected key \"%s\" to exist, but it didn't", k)
	}
	if v != val {
		t.Errorf("Expected cache value \"%s\" to equal \"%f\", was %f", k, v, val)
	}
}

func TestInMemoryCache_GetStringMap(t *testing.T) {
	color, hex := "yellow", "#FFFF00"
	k, v := "colors", map[string]string{color: hex}
	cache := InMemoryCache{cache: map[string]interface{}{k: v}}
	val, err := cache.GetStringMap(k)
	if err != nil {
		t.Error(err)
		t.Errorf("Expected key \"%s\" to exist, but it didn't", k)
	}
	c, ok := val[color]
	if !ok {
		t.Errorf("Expected key \"%s\" to exist, but it didn't", color)
	}
	if hex != c {
		t.Errorf("Expected cache value \"%s\" to equal \"%s\", was %s", color, hex, c)
	}
}

func TestInMemoryCache_GetStringSlice(t *testing.T) {
	primes, nums := "primes", []string{"1", "3", "5", "7"}
	cache := InMemoryCache{cache: map[string]interface{}{primes: nums}}
	ns, err := cache.GetStringSlice(primes)
	if err != nil {
		t.Errorf("Expected key \"%s\" to exist, but it didn't", primes)
	}
	for i, n := range ns {
		if nums[i] != n {
			t.Errorf("Expected cache value \"%s\" to equal \"%s\", was %s", primes, nums[i], n)
		}
	}
}

func TestInMemoryCache_GetKeys(t *testing.T) {
	m := map[string]interface{}{
		"1": 1,
		"2": 2,
		"3": 3,
	}
	cache := InMemoryCache{cache: m}
	keys, err := cache.Keys()
	if err != nil {
		t.Error(err)
	}

	sort.Strings(keys)
	for i, k := range keys {
		e := fmt.Sprintf("%d", i+1)
		if k != e {
			t.Errorf("Expected key to equal %s, was %s", e, k)
		}
	}
}
