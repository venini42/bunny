package stores

import (
	"github.com/gomodule/redigo/redis"
)

type RedisCache struct {
	pool *redis.Pool
}

func (c *RedisCache) Set(key string, value interface{}) error {
	conn := c.pool.Get()
	defer conn.Close()
	_, err := conn.Do("SET", key, value)
	return err
}

func (c *RedisCache) Get(key string) (interface{}, error) {
	conn := c.pool.Get()
	defer conn.Close()
	return conn.Do("GET", key)
}

func (c *RedisCache) GetBool(key string) (bool, error) {
	return redis.Bool(c.Get(key))
}

func (c *RedisCache) GetFloat64(key string) (float64, error) {
	return redis.Float64(c.Get(key))
}

func (c *RedisCache) GetInt(key string) (int, error) {
	return redis.Int(c.Get(key))
}

func (c *RedisCache) GetString(key string) (string, error) {
	return redis.String(c.Get(key))
}

func (c *RedisCache) GetStringMap(key string) (map[string]string, error) {
	return redis.StringMap(c.Get(key))
}

func (c *RedisCache) GetStringSlice(key string) ([]string, error) {
	return redis.Strings(c.Get(key))
}

func (c *RedisCache) Keys() ([]string, error) {
	conn := c.pool.Get()
	defer conn.Close()
	return redis.Strings(conn.Do("KEYS", "*"))
}

func Redis(addr, pwd string, db int) (*RedisCache, error) {
	pool := &redis.Pool{
		Dial: func() (redis.Conn, error) {
			return redis.DialURL(addr, redis.DialPassword(pwd), redis.DialDatabase(db))
		},
	}
	_, err := pool.Get().Do("PING")
	if err != nil {
		return nil, err
	}
	return &RedisCache{pool: pool}, nil
}
