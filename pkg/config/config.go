package config

import (
	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
	"time"
)

type Configuration interface {
	Get(key string) interface{}
	GetBool(key string) bool
	GetFloat64(key string) float64
	GetInt(key string) int
	GetString(key string) string
	GetStringMap(key string) map[string]interface{}
	GetStringMapString(key string) map[string]string
	GetStringSlice(key string) []string
	GetTime(key string) time.Time
	GetDuration(key string) time.Duration
	IsSet(key string) bool
	AllSettings() map[string]interface{}
}

func New(prefix string, keys KeyList, providers ProviderList) (Configuration, error) {
	conf := viper.New()
	conf.SetEnvPrefix(prefix)

	for _, key := range keys {
		conf.SetDefault(key.key, key.defaultValue)
	}

	for _, provider := range providers {
		err := provider.Install(keys.keys(), conf)
		if err != nil {
			return nil, err
		}
	}
	var c interface{} = conf // Yay for type casting in go
	return c.(Configuration), nil
}
