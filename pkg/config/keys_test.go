package config

import "testing"

func TestConfigKey(t *testing.T) {
	k := Key("test", "test")
	if k.key != "test" {
		t.Errorf("Expected key to equal \"test\", was %s", k.key)
	}

	if k.defaultValue != "test" {
		t.Errorf("Expected default value to equal \"test\", was %s", k.defaultValue)
	}
}

func TestKeys(t *testing.T) {
	ks := Keys(
		Key("1", "1"),
		Key("2", "2"),
	)

	keys := ks.keys()
	if len(keys) != len(ks) {
		t.Errorf("Expected length of string array to be %d, was %d", len(ks), len(keys))
	}

	for i, k := range keys {
		if exp := ks[i].key; k != exp {
			t.Errorf("Expected key at index %d to equal %s, was %s", i, exp, k)
		}
	}
}
