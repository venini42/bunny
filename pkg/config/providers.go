package config

import (
	"github.com/spf13/viper"
)

type Provider interface {
	Install(keys []string, conf *viper.Viper) error
}

type ProviderList []Provider

func Providers(provs ...Provider) ProviderList {
	return provs
}
