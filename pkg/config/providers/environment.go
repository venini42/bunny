package providers

import (
	"github.com/spf13/viper"
	"strings"
)

type EnvironmentProvider struct {
}

func (e *EnvironmentProvider) Install(keys []string, conf *viper.Viper) error {
	conf.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	for _, key := range keys {
		err := conf.BindEnv(key)
		if err != nil {
			return err
		}
	}
	return nil
}

func Environment() *EnvironmentProvider {
	return &EnvironmentProvider{}
}
