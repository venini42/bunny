package providers

import "github.com/spf13/viper"

type ConsulProvider struct {
	Url        string
	Key        string
	ConfigType string
}

func (c *ConsulProvider) Install(keys []string, conf *viper.Viper) error {
	err := conf.AddRemoteProvider("consul", c.Url, c.Key)
	if err != nil {
		return err
	}

	conf.SetConfigType(c.ConfigType)
	return conf.ReadRemoteConfig()
}

func Consul(url, key, format string) *ConsulProvider {
	return &ConsulProvider{
		Url:        url,
		Key:        key,
		ConfigType: format,
	}
}
