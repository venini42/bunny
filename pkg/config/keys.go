package config

type KeyList []KeyPair

type KeyPair struct {
	key          string
	defaultValue interface{}
}

func Key(key string, defaultValue interface{}) KeyPair {
	return KeyPair{
		key:          key,
		defaultValue: defaultValue,
	}
}

func (k KeyList) keys() []string {
	var keys []string
	for _, key := range k {
		keys = append(keys, key.key)
	}
	return keys
}

func Keys(keys ...KeyPair) KeyList {
	return keys
}
