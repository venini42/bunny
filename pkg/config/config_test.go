package config

import (
	"gitlab.com/venini42/bunny/pkg/config/providers"
	"os"
	"testing"
)

func TestConfig(t *testing.T) {
	provs := Providers(
		providers.Environment(),
	)
	keys := Keys(
		Key("hello.world", "golang"),
		Key("a", "A"),
		Key("b", 1),
	)

	conf, err := New("", keys, provs)
	if err != nil {
		t.Fatal(err)
	}

	expectedStr := "golang"
	actualStr := conf.GetString("hello.world")
	if actualStr != expectedStr {
		t.Errorf("Expected 'hello.world' variable to equal %s, was %s", expectedStr, actualStr)
	}

	expectedStr = "world"
	err = os.Setenv("HELLO_WORLD", expectedStr)
	if err != nil {
		t.Fatal(err)
	}

	actualStr = conf.GetString("hello.world")
	if actualStr != expectedStr {
		t.Errorf("Expected 'hello.world' variable to equal %s, was %s", expectedStr, actualStr)
	}

	expectedStr = "A"
	actualStr = conf.GetString("a")
	if actualStr != expectedStr {
		t.Errorf("Expected 'a' variable to equal %s, was %s", expectedStr, actualStr)
	}

	expectedInt := 1
	actualInt := conf.GetInt("b")
	if actualInt != expectedInt {
		t.Errorf("Expected 'b' variable to equal %d, was %d", expectedInt, actualInt)
	}

	expectedInt = 0
	actualInt = conf.GetInt("what")
	if actualInt != expectedInt {
		t.Errorf("Expected non existent integer variable to equal %d, was %d", expectedInt, actualInt)
	}
}
