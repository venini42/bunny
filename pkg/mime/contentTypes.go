package mime

type mimeTypes struct {
	Protobuf string
}

var MimeTypes = mimeTypes{
	Protobuf: "application/vnd.google.protobuf",
}
