package context

import (
	goctx "context"
)

const replyToKey = "bunny.ctx.reply-to"

func WithReplyTo(ctx goctx.Context, replyTo string) goctx.Context {
	return goctx.WithValue(ctx, replyToKey, replyTo)
}

func GetReplyTo(ctx goctx.Context) string {
	value := ctx.Value(replyToKey)
	if value != nil {
		return value.(string)
	}
	return ""
}
