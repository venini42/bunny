package context

import (
	"context"
	"testing"
)

const correlationId = "test"

func TestWithCorrelationId(t *testing.T) {
	ctx := context.Background()
	ctx = WithCorrelationId(ctx, correlationId)
	id := ctx.Value(correlationIdKey)
	if id == nil {
		t.Errorf("Expected CorrelationId to equal %s, was nil", correlationId)
	}

	if id.(string) != correlationId {
		t.Errorf("Expected CorrelationId to equal %s, was %s", correlationId, id)
	}
}

func TestGetCorrelationId(t *testing.T) {
	ctx := context.WithValue(context.Background(), correlationIdKey, correlationId)
	id := GetCorrelationId(ctx)
	if id != correlationId {
		t.Errorf("Expected CorrelationId to equal %s, was %s", correlationId, id)
	}
}
