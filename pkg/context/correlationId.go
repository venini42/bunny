package context

import (
	goctx "context"
)

const correlationIdKey = "bunny.ctx.correlationId"

func WithCorrelationId(ctx goctx.Context, correlationId string) goctx.Context {
	return goctx.WithValue(ctx, correlationIdKey, correlationId)
}

func GetCorrelationId(ctx goctx.Context) string {
	value := ctx.Value(correlationIdKey)
	if value != nil {
		return value.(string)
	}
	return ""
}
