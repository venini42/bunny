package context

import (
	"context"
	"testing"
)

const replyTo = "reply"

func TestWithReplyTo(t *testing.T) {
	ctx := context.Background()
	ctx = WithReplyTo(ctx, replyTo)
	actual := ctx.Value(replyToKey)
	if actual == nil {
		t.Errorf("Expected ReplyTo to equal %s, was nil", correlationId)
	}

	if actual.(string) != replyTo {
		t.Errorf("Expected ReplyTo to equal %s, was %s", replyTo, actual)
	}
}

func TestGetReplyTo(t *testing.T) {
	ctx := context.WithValue(context.Background(), replyToKey, replyTo)
	actual := GetReplyTo(ctx)
	if actual != replyTo {
		t.Errorf("Expected ReplyTo to equal %s, was %s", replyTo, actual)
	}
}
