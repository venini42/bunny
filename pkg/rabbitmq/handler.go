package rabbitmq

import (
	"context"
)

type MessageHandler func(ctx context.Context, delivery Delivery, reply ReplyFunc) error
