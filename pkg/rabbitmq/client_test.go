package rabbitmq

import (
	"context"
	"errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	bunnyctx "gitlab.com/venini42/bunny/pkg/context"
	"gitlab.com/venini42/bunny/pkg/rabbitmq/topology"
	"gitlab.com/venini42/bunny/pkg/tests"
	"os"
	"testing"
	"time"
)

var url string
var sut *RabbitClient

const queueName = "test"
const exchangeName = "testing"
const topic = "topics:test"

func withTestQueue(t *testing.T, test func(t *testing.T, fail func(err error))) {
	fail := tests.FailOnError(t)
	_, err := sut.channel.QueueDeclare(queueName, false, false, false, false, nil)
	fail(err)
	err = sut.channel.ExchangeDeclare(exchangeName, "topic", false, false, false, false, nil)
	fail(err)
	err = sut.channel.QueueBind(queueName, topic, exchangeName, false, nil)
	fail(err)

	test(t, fail)

	err = sut.channel.QueueUnbind(queueName, topic, exchangeName, nil)
	fail(err)
	err = sut.channel.ExchangeDelete(exchangeName, false, false)
	fail(err)
	_, err = sut.channel.QueueDelete(queueName, false, false, false)
	fail(err)
}

func TestMain(m *testing.M) {
	url = os.Getenv("AMQP_URL")
	if len(url) < 1 {
		url = "amqp://guest:guest@localhost:5672/"
	}
	logger := logrus.New()
	client, err := NewClient(url, logger, 5)
	if err != nil {
		panic(err)
	}
	err = client.Connect()
	if err != nil {
		panic(err)
	}

	sut = client
	code := m.Run()
	err = client.Stop()
	if err != nil {
		panic(err)
	}
	os.Exit(code)
}

func TestRabbitClient_Init(t *testing.T) {
	fail := tests.FailOnError(t)
	err := sut.Init(func(builder topology.Builder) error {

		queue, err := builder.Queue(queueName)
		fail(err)
		exchange, err := builder.Exchange(exchangeName,
			topology.ExchangeKind(topology.TopicExchange),
		)
		fail(err)
		return builder.Binding(
			topology.BindingQueue(queue),
			topology.BindingExchange(exchange),
			topology.BindingKey(topic),
		)
	})

	fail(err)
	_, err = sut.channel.QueueDeclarePassive(queueName, false, false, false, false, nil)
	fail(err)
	err = sut.channel.ExchangeDeclarePassive(exchangeName, "topic", false, false, false, false, nil)
	fail(err)
	err = sut.channel.QueueUnbind(queueName, topic, exchangeName, nil)
	fail(err)
	err = sut.channel.ExchangeDelete(exchangeName, false, false)
	fail(err)
	_, err = sut.channel.QueueDelete(queueName, false, false, false)
	fail(err)
}

func TestRabbitClient_Cast(t *testing.T) {
	withTestQueue(t, func(t *testing.T, fail func(err error)) {
		deliveries, err := sut.channel.Consume(queueName, "tester", true, true, false, false, nil)
		fail(err)

		body := "test"
		req := Delivery{
			Body:       []byte(body),
			Exchange:   exchangeName,
			RoutingKey: topic,
		}
		err = sut.Cast(context.TODO(), req)
		fail(err)

		msg := <-deliveries
		response := string(msg.Body)
		fail(err)
		if response != body {
			t.Errorf("Response should equal %s, was %s", body, response)
		}
	})
}

func TestRabbitClient_Call(t *testing.T) {
	withTestQueue(t, func(t *testing.T, fail func(err error)) {
		deliveries, err := sut.channel.Consume(queueName, "tester", true, true, false, false, nil)
		fail(err)

		body := "request"
		response := "response"
		go func() {
			msg := <-deliveries
			request := string(msg.Body)
			if request != body {
				t.Errorf("Request should equal %s, was %s", body, request)
			}

			resp := amqp.Publishing{
				Timestamp: time.Now(),
				Body:      []byte(response),
			}

			err = sut.channel.Publish("", msg.ReplyTo, false, false, resp)
			fail(err)
		}()

		req := Delivery{
			Body:       []byte(body),
			Exchange:   exchangeName,
			RoutingKey: topic,
		}
		r, err := sut.Call(context.TODO(), req)
		fail(err)
		resp := string(r.Body)
		if resp != response {
			t.Errorf("Response should equal %s, was %s", response, resp)
		}
	})
}

func TestRabbitClient_Listen(t *testing.T) {
	message := "Hello World!"
	withTestQueue(t, func(t *testing.T, fail func(err error)) {
		ctx := context.Background()
		errs := sut.Listen(ctx, queueName, false, false, false, false, nil, func(ctx context.Context, delivery Delivery, _ ReplyFunc) error {
			if correlationId := bunnyctx.GetCorrelationId(ctx); correlationId != "1" {
				t.Errorf("Expected to receive correlationId \"1\", actual %s", correlationId)
			}

			if replyTo := bunnyctx.GetReplyTo(ctx); replyTo != "test" {
				t.Errorf("Expected to receive replyTo \"test\", actual %s", replyTo)
			}

			if header := delivery.Headers["hello"]; header != "world" {
				t.Errorf("Expected to receive hello header \"world\", actual %s", header)
			}

			if exchange := delivery.Exchange; exchange != exchangeName {
				t.Errorf("Expected to receive from exchange %s, actual %s", exchangeName, exchange)
			}

			if key := delivery.RoutingKey; key != topic {
				t.Errorf("Expected to receive routing key %s, actual %s", topic, key)
			}

			if actual := string(delivery.Body); actual != message {
				t.Errorf("Expected to receive %s, actual %s", message, actual)
			}
			return errors.New("error")
		})
		go func() {
			err := sut.channel.Publish(exchangeName, topic, false, false, amqp.Publishing{
				CorrelationId: "1",
				ReplyTo:       "test",
				Headers: amqp.Table{
					"hello": "world",
				},
				Body: []byte(message),
			})
			fail(err)
		}()
		err := <-errs
		if err == nil {
			t.Errorf("Expected error, got %s", err)
		}
	})
}
