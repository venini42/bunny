package rabbitmq

import (
	"github.com/streadway/amqp"
)

type Delivery struct {
	Headers    amqp.Table
	Body       []byte
	Exchange   string
	RoutingKey string
}

func fromAmqpDelivery(delivery amqp.Delivery) Delivery {
	return Delivery{
		Headers:    delivery.Headers,
		Body:       delivery.Body,
		Exchange:   delivery.Exchange,
		RoutingKey: delivery.RoutingKey,
	}
}

func fromResponse(exchange, routingKey string, response Response) Delivery {
	return Delivery{
		Headers:    response.Headers,
		Body:       response.Body,
		Exchange:   exchange,
		RoutingKey: routingKey,
	}
}
