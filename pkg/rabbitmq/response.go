package rabbitmq

import (
	"github.com/streadway/amqp"
)

type Response struct {
	Headers amqp.Table
	Body    []byte
}

func fromAmqpResponse(delivery amqp.Delivery) Response {
	return Response{
		Headers: delivery.Headers,
		Body:    delivery.Body,
	}
}
