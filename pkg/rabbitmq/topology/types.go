package topology

type exchangeKind string

const (
	DirectExchange  = exchangeKind("direct")
	FanoutExchange  = exchangeKind("fanout")
	TopicExchange   = exchangeKind("topic")
	HeadersExchange = exchangeKind("headers")
)

type ExchangeKindStr interface {
	Kind() string
}

func (k exchangeKind) Kind() string {
	return string(k)
}
