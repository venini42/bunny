package topology

import "github.com/streadway/amqp"

type QueueOptions struct {
	durable,
	autoDelete,
	exclusive,
	noWait bool
	args amqp.Table
}

type QueueOption func(opts *QueueOptions)

func QueueDurable(durable bool) QueueOption {
	return func(opts *QueueOptions) {
		opts.durable = durable
	}
}

func QueueAutoDelete(autoDelete bool) QueueOption {
	return func(opts *QueueOptions) {
		opts.autoDelete = autoDelete
	}
}

func QueueExclusive(exclusive bool) QueueOption {
	return func(opts *QueueOptions) {
		opts.exclusive = exclusive
	}
}

func QueueNoWait(noWait bool) QueueOption {
	return func(opts *QueueOptions) {
		opts.noWait = noWait
	}
}

func QueueArgs(args amqp.Table) QueueOption {
	return func(opts *QueueOptions) {
		opts.args = args
	}
}

func (t Builder) Queue(name string, opts ...QueueOption) (string, error) {
	opt := &QueueOptions{
		durable:    false,
		autoDelete: false,
		exclusive:  false,
		noWait:     false,
		args:       nil,
	}

	for _, o := range opts {
		o(opt)
	}
	q, err := t.Channel.QueueDeclare(name, opt.durable, opt.autoDelete, opt.exclusive, opt.noWait, opt.args)
	if err != nil {
		return "", err
	}

	return q.Name, nil
}
