package topology

import "github.com/streadway/amqp"

type Builder struct {
	Channel *amqp.Channel
}
