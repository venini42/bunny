package topology

import "github.com/streadway/amqp"

type BindingOptions struct {
	queueName,
	key,
	exchangeName string
	noWait bool
	args   amqp.Table
}

type BindingOption func(options *BindingOptions)

func BindingKey(key string) BindingOption {
	return func(opts *BindingOptions) {
		opts.key = key
	}
}

func BindingQueue(name string) BindingOption {
	return func(opts *BindingOptions) {
		opts.queueName = name
	}
}

func BindingExchange(name string) BindingOption {
	return func(opts *BindingOptions) {
		opts.exchangeName = name
	}
}

func BindingNoWait(noWait bool) BindingOption {
	return func(opts *BindingOptions) {
		opts.noWait = noWait
	}
}

func BindingArgs(args amqp.Table) BindingOption {
	return func(opts *BindingOptions) {
		opts.args = args
	}
}

func (t Builder) Binding(opts ...BindingOption) error {
	opt := &BindingOptions{
		noWait: false,
		args:   nil,
	}

	for _, o := range opts {
		o(opt)
	}

	return t.Channel.QueueBind(opt.queueName, opt.key, opt.exchangeName, opt.noWait, opt.args)
}
