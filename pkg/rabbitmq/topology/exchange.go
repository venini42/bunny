package topology

import (
	"errors"
	"github.com/streadway/amqp"
)

type ExchangeOptions struct {
	kind ExchangeKindStr
	durable,
	autoDelete,
	internal,
	noWait bool
	args amqp.Table
}

type ExchangeOption func(opts *ExchangeOptions)

func ExchangeKind(kind ExchangeKindStr) ExchangeOption {
	return func(opts *ExchangeOptions) {
		opts.kind = kind
	}
}

func ExchangeDurable(durable bool) ExchangeOption {
	return func(opts *ExchangeOptions) {
		opts.durable = durable
	}
}

func ExchangeAutoDelete(autoDelete bool) ExchangeOption {
	return func(opts *ExchangeOptions) {
		opts.autoDelete = autoDelete
	}
}

func ExchangeInternal(internal bool) ExchangeOption {
	return func(opts *ExchangeOptions) {
		opts.internal = internal
	}
}

func ExchangeNoWait(noWait bool) ExchangeOption {
	return func(opts *ExchangeOptions) {
		opts.noWait = noWait
	}
}

func ExchangeArgs(args amqp.Table) ExchangeOption {
	return func(opts *ExchangeOptions) {
		opts.args = args
	}
}

func (t Builder) Exchange(name string, opts ...ExchangeOption) (string, error) {
	opt := &ExchangeOptions{
		kind:       nil,
		durable:    false,
		autoDelete: false,
		internal:   false,
		noWait:     false,
		args:       nil,
	}

	for _, o := range opts {
		o(opt)
	}
	if opt.kind == nil {
		return "", errors.New("no ExchangeKind specified")
	}

	err := t.Channel.ExchangeDeclare(name, opt.kind.Kind(), opt.durable, opt.autoDelete, opt.internal, opt.noWait, opt.args)
	if err != nil {
		return "", err
	}
	return name, nil
}
