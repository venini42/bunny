package rabbitmq

import (
	"context"
	"github.com/streadway/amqp"
)

type Client interface {
	Connect() error
	Init(initializer TopologyInitializer) error
	Cast(ctx context.Context, request Delivery) error
	Call(ctx context.Context, request Delivery) (Response, error)
	Listen(ctx context.Context, queue string, autoAck, exclusive, noLocal, noWait bool, args amqp.Table, handler MessageHandler) chan error
	Stop() error
}
