package rabbitmq

import (
	"context"
	"errors"
	bunnyctx "gitlab.com/venini42/bunny/pkg/context"
)

type ReplyFunc func(ctx context.Context, response Response) error

func CreateReplyFunc(client *RabbitClient) ReplyFunc {
	return func(ctx context.Context, response Response) error {
		replyTo := bunnyctx.GetReplyTo(ctx)
		if len(replyTo) < 0 {
			return errors.New("cannot reply without a ReplyTo param")
		}

		request := fromResponse("", replyTo, response)
		err := client.Cast(ctx, request)
		return err
	}
}
