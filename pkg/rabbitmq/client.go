package rabbitmq

import (
	"context"
	"errors"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	bunnyctx "gitlab.com/venini42/bunny/pkg/context"
	"gitlab.com/venini42/bunny/pkg/mime"
	"gitlab.com/venini42/bunny/pkg/rabbitmq/topology"
	"time"
)

const rabbitDirectReplyTo = "amq.rabbitmq.reply-to"

type RabbitClient struct {
	brokerUrl  string
	connection *amqp.Connection
	channel    *amqp.Channel
	logger     log.FieldLogger
	consumers  []string
	timeout    time.Duration
}

type TopologyInitializer func(builder topology.Builder) error

/*
Create a new RabbitMQ Client with the given AMQP URL (e.g. amqp://localhost:5672/), logger instance
and timeout (in seconds)
*/
func NewClient(url string, logger log.FieldLogger, timeout int) (*RabbitClient, error) {
	client := &RabbitClient{
		brokerUrl: url,
		logger:    logger,
		timeout:   time.Duration(timeout) * time.Second,
	}
	return client, nil
}

func (client *RabbitClient) Connect() error {
	client.logger.Info("Connecting to broker...")
	connection, err := amqp.Dial(client.brokerUrl)
	if err != nil {
		client.logger.WithField("error", err).Error("Connection failed")
		return err
	}

	client.connection = connection
	channel, err := connection.Channel()
	if err != nil {
		client.logger.WithField("error", err).Error("Channel creation failed")
		return err
	}

	client.channel = channel
	client.logger.Info("Connection successful")
	return nil
}

func (client *RabbitClient) Init(initializer TopologyInitializer) error {
	client.logger.Info("Initializing RabbitMQ topology, creating temp channel...")
	if client.connection == nil {
		return errors.New("broker connection not established. Run Connect() before Init()")
	}

	ch, err := client.connection.Channel()
	if err != nil {
		client.logger.WithField("error", err).Error("Temp channel creation failed")
		return err
	}

	defer ch.Close()
	err = initializer(topology.Builder{Channel: ch})
	if err != nil {
		return err
	}

	client.logger.Info("Topology initialized. Closing temp channel...")
	return nil
}

/*
Perform an async publishing
*/
func (client *RabbitClient) Cast(ctx context.Context, request Delivery) error {
	correlationId := bunnyctx.GetCorrelationId(ctx)
	replyTo := bunnyctx.GetReplyTo(ctx)
	return client.doPublish(request.Exchange, request.RoutingKey, correlationId, request.Headers, request.Body, replyTo)
}

/*
Perform a Direct ReplyTo call, automatically listening for a response
*/
func (client *RabbitClient) Call(ctx context.Context, request Delivery) (Response, error) {
	consumer := uuid.New().String()
	deliveries, err := client.channel.Consume(
		rabbitDirectReplyTo,
		consumer,
		true,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		client.logger.WithField("error", err).Error("Failed to attach consumer for ReplyTo")
		return Response{}, err
	}

	correlationId := bunnyctx.GetCorrelationId(ctx)
	err = client.doPublish(request.Exchange, request.RoutingKey, correlationId, request.Headers, request.Body, rabbitDirectReplyTo)
	if err != nil {
		return Response{}, err
	}

	ctx, cancel := context.WithTimeout(ctx, client.timeout)
	defer cancel()

	cancelConsumer := func() {
		err := client.channel.Cancel(consumer, false)
		if err != nil {
			client.logger.WithField(
				"error",
				err,
			).Warnf("Could not cancel consumer %s", consumer)
		}
	}
	select {
	case msg := <-deliveries:
		cancelConsumer()
		return fromAmqpResponse(msg), nil
	case <-ctx.Done():
		cancelConsumer()
		return Response{}, ctx.Err()
	}
}

func (client *RabbitClient) Listen(ctx context.Context, queue string, autoAck, exclusive, noLocal, noWait bool, args amqp.Table, handler MessageHandler) chan error {
	errs := make(chan error)

	consumerId := uuid.New().String()
	deliveries, err := client.channel.Consume(queue, consumerId, autoAck, exclusive, noLocal, noWait, args)
	if err != nil {
		errs <- err
		return errs
	}

	client.consumers = append(client.consumers, consumerId)

	go func() {
		for msg := range deliveries {
			ctx = bunnyctx.WithCorrelationId(ctx, msg.CorrelationId)
			ctx = bunnyctx.WithReplyTo(ctx, msg.ReplyTo)

			delivery := fromAmqpDelivery(msg)
			err := handler(ctx, delivery, CreateReplyFunc(client))
			if err != nil {
				errs <- err
				return
			}
		}
	}()

	go func() {
		<-ctx.Done()
		err := client.channel.Cancel(consumerId, false)
		if err != nil {
			errs <- err
		}
	}()

	return errs
}

func (client *RabbitClient) doPublish(exchange, routingKey, correlationId string, headers amqp.Table, body []byte, replyTo string) error {
	msg := amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		Timestamp:    time.Now(),
		ContentType:  mime.MimeTypes.Protobuf,
		Headers:      headers,
		Body:         body,
	}

	if len(replyTo) > 0 {
		msg.ReplyTo = replyTo
	}

	err := client.channel.Publish(exchange, routingKey, false, false, msg)
	if err != nil {
		client.logger.WithFields(log.Fields{
			"error":    err,
			"exchange": exchange,
			"key":      routingKey,
			"body":     body,
		}).Error("Failed to publish message")
		return err
	}
	return nil
}

func (client *RabbitClient) Stop() error {
	client.logger.Info("Stopping RabbitMQ client...")
	for _, consumer := range client.consumers {
		err := client.channel.Cancel(consumer, false)
		if err != nil {
			return err
		}
	}

	err := client.channel.Close()
	if err != nil {
		client.logger.WithField("error", err).Fatal("Channel teardown failed")
		return err
	}

	err = client.connection.Close()
	if err != nil {
		client.logger.WithField("error", err).Fatal("Connection teardown failed")
		return err
	}
	client.logger.Info("RabbitMQ client stopped")
	return nil
}
