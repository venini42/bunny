test: fmt vet
	go test ./pkg/... ./

fmt:
	go fmt ./pkg/... ./

vet:
	go vet ./pkg/... ./

