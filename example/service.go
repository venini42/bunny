package main

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/venini42/bunny"
	"gitlab.com/venini42/bunny/pkg/cache"
	"gitlab.com/venini42/bunny/pkg/cache/stores"
	"gitlab.com/venini42/bunny/pkg/config"
	"gitlab.com/venini42/bunny/pkg/config/providers"
	bunnyctx "gitlab.com/venini42/bunny/pkg/context"
	"gitlab.com/venini42/bunny/pkg/rabbitmq"
	"os"
)

var conf config.Configuration
var cch cache.Store

func init() {
	provs := config.Providers(
		providers.Environment(),
		// providers.Consul("localhost:8500", "/config/global.json", "json"),
	)

	keys := config.Keys(
		config.Key("broker.url", "amqp://guest:guest@localhost:5672/"),
	)

	c, err := config.New("", keys, provs)
	if err != nil {
		panic(err)
	}
	conf = c
	cch = stores.InMemory()
}

func main() {
	logger := logrus.New()
	service, err := bunny.NewService(
		bunny.ServiceName("example"),
		bunny.ServiceLogger(logger),
		bunny.ServiceBrokerUrl(conf.GetString("broker.url")),
		bunny.ServiceCache(cch),
	)
	if err != nil {
		logger.Fatal(err)
	}
	ctx := context.Background()
	service.Logger.Info(bunnyctx.GetReplyTo(ctx))

	service.AddListener("test", "test", func(ctx context.Context, message rabbitmq.Delivery, _ rabbitmq.ReplyFunc) error {
		service.Logger.Infof("Received message on topic %s: %s", message.RoutingKey, string(message.Body))
		return nil
	})

	service.AddListener("hello",
		"world", func(ctx context.Context, request rabbitmq.Delivery, reply rabbitmq.ReplyFunc) error {
			req := string(request.Body)
			cached, err := service.Cache.GetString("req")
			if err != nil || cached == "" {
				_ = service.Cache.Set("req", req)
				cached = req
				service.Logger.Infof("Cached request with content %s", req)
			} else {
				service.Logger.Infof("Found cached request with value %s", cached)
			}

			service.Logger.Infof("Received message on topic %s: %s", request.RoutingKey, cached)

			response := rabbitmq.Response{
				Body: []byte("hello to you!"),
			}
			return reply(ctx, response)
		})

	err = service.Start()
	if err != nil {
		logger.Fatal(err)
	}
	os.Exit(0)
}
