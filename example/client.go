package main

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/venini42/bunny/pkg/rabbitmq"
	"os"
	"time"
)

func main() {
	logger := logrus.New()
	client, err := rabbitmq.NewClient("amqp://guest:guest@localhost:5672", logger, 5)
	panicOnError(err)

	err = client.Connect()
	panicOnError(err)

	// Send a message and move on
	request := rabbitmq.Delivery{
		Body:       []byte("yellow"),
		Exchange:   "test",
		RoutingKey: "test",
	}
	err = client.Cast(context.Background(), request)
	panicOnError(err)

	ctx := context.Background()
	// Set a timeout because we don't want to hang if the service is not there
	ctx, cancel := context.WithTimeout(ctx, time.Duration(5)*time.Second)
	defer cancel()
	done := make(chan int)

	go func() {
		// Send a message and wait for a reply
		req := rabbitmq.Delivery{
			Body:       []byte("hello bunny!"),
			Exchange:   "hello",
			RoutingKey: "world",
		}
		res, err := client.Call(ctx, req)
		panicOnError(err)
		response := string(res.Body)
		logger.Infof("Received response: %s", response)
		done <- 0
	}()

	select {
	case <-done:
	case <-ctx.Done():
		logger.Fatal(ctx.Err())
	}

	os.Exit(0)
}

func panicOnError(err error) {
	if err != nil {
		logrus.Fatal(err)
	}
}
