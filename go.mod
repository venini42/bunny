module gitlab.com/venini42/bunny

go 1.12

require (
	github.com/coreos/etcd v3.3.15+incompatible // indirect; Work around this https://github.com/spf13/viper/issues/658
	github.com/etherlabsio/healthcheck v0.0.0-20190516102650-2b759a75f4be
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/viper v1.4.0
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
	golang.org/x/sys v0.0.0-20190710143415-6ec70d6a5542 // indirect
	sigs.k8s.io/yaml v1.1.0 // indirect
)
