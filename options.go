package bunny

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/venini42/bunny/pkg/cache"
)

type ServiceOptions struct {
	name,
	brokerUrl string
	logger          logrus.FieldLogger
	timeout         int
	cache           cache.Store
	healthCheckPort int
}

type ServiceOption func(opts *ServiceOptions)

func ServiceName(name string) ServiceOption {
	return func(opts *ServiceOptions) {
		opts.name = name
	}
}

func ServiceBrokerUrl(url string) ServiceOption {
	return func(opts *ServiceOptions) {
		opts.brokerUrl = url
	}
}

func ServiceLogger(logger logrus.FieldLogger) ServiceOption {
	return func(opts *ServiceOptions) {
		opts.logger = logger
	}
}

func ServiceTimeout(seconds int) ServiceOption {
	return func(opts *ServiceOptions) {
		opts.timeout = seconds
	}
}

func ServiceCache(cache cache.Store) ServiceOption {
	return func(opts *ServiceOptions) {
		opts.cache = cache
	}
}

func ServiceHealthCheckPort(port int) ServiceOption {
	return func(opts *ServiceOptions) {
		opts.healthCheckPort = port
	}
}
